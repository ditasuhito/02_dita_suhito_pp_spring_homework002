package com.example.springhomework002.Model.Entity;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Invoice {
    private Integer invoice_id;
    private LocalDateTime invoice_date;
    private Customer customer;
    private List<Product> product;

}
