package com.example.springhomework002.Model.Entity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer product_id;
    private String product_name;
    private String product_price;
}
