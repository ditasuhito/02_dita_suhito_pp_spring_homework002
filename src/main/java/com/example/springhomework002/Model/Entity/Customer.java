package com.example.springhomework002.Model.Entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Customer {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customer_id;
    private String customer_name;
    private String customer_address;
    private String customer_phone;

}