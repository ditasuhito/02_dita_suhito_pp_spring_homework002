package com.example.springhomework002.Model.Response;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;
import java.time.LocalDateTime;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Responder<T> {
    private LocalDateTime datee;
    private HttpStatus status;
    private String mes;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;

}