package com.example.springhomework002.Model.Request;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomerRequest {
    private String name;
    private String address;
    private String phone;
}

