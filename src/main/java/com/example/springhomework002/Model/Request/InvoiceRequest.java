package com.example.springhomework002.Model.Request;

import java.time.LocalDateTime;
import java.util.List;

public class InvoiceRequest {
    private LocalDateTime invoiceDate;
    private List<Integer> productId;
    private Integer customerId;
}
