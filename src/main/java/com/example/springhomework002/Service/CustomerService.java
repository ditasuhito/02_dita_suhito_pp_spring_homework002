package com.example.springhomework002.Service;

import com.example.springhomework002.Model.Entity.Customer;
import com.example.springhomework002.Model.Request.CustomerRequest;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomer();

    Customer getCustomerId(Integer id);

    Customer insertCustomer(CustomerRequest customerRequest);

    Customer updatebyID(Integer id,CustomerRequest customerRequest);

   void deleteCustomer(Integer id);
}
