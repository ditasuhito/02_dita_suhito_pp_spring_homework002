package com.example.springhomework002.Service;

import com.example.springhomework002.Model.Entity.Invoice;
import com.example.springhomework002.Model.Request.InvoiceRequest;
import com.example.springhomework002.Repositery.InvoiceRepo;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepo invoiceRepo;

    public InvoiceServiceImp(InvoiceRepo invoiceRepo) {
        this.invoiceRepo = invoiceRepo;
    }

    @Override
    public Invoice insertInvoice(InvoiceRequest invoiceRequest) {
        return invoiceRepo.insertData(invoiceRequest);
    }

    @Override
    public Invoice getInvoiceId(Integer id) {
        return invoiceRepo.getById(id);
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepo.getAllInvoice();
    }

    @Override
    public void deleteInvoice(Integer id) {
        invoiceRepo.deleteInvoice(id);
    }

    @Override
    public Invoice updateInvoice(Integer id, InvoiceRequest invoiceRequest) {
        return invoiceRepo.updateInvoice(id,invoiceRequest);
    }
}
