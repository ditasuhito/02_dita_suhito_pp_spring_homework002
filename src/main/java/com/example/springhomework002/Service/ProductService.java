package com.example.springhomework002.Service;

import com.example.springhomework002.Model.Entity.Product;
import com.example.springhomework002.Model.Request.ProductRequest;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

    Product getProductId(Integer id);

    Product insertPro(ProductRequest productRequest);

    void deleteProduct(Integer id);

    Product updatebyID(Integer id,ProductRequest productRequest);
}
