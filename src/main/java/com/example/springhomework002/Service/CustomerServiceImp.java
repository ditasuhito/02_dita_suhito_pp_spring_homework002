package com.example.springhomework002.Service;

import com.example.springhomework002.Model.Entity.Customer;
import com.example.springhomework002.Model.Request.CustomerRequest;
import com.example.springhomework002.Repositery.CustomerRepo;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class CustomerServiceImp implements CustomerService{

    private final CustomerRepo customerRepo;

    public CustomerServiceImp(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepo.getAllCustomer();
    }

    @Override
    public Customer getCustomerId(Integer id) {
        return customerRepo.getByiD(id);
    }

    @Override
    public Customer insertCustomer(CustomerRequest customerRequest) {
        return customerRepo.insertData(customerRequest);
    }

    @Override
    public Customer updatebyID(Integer id, CustomerRequest customerRequest) {
        return customerRepo.updatebyID(id,customerRequest);
    }

    @Override
    public void deleteCustomer(Integer id) {
        customerRepo.deleteCustomer(id);
    }


}
