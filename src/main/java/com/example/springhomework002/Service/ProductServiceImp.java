package com.example.springhomework002.Service;

import com.example.springhomework002.Model.Entity.Product;
import com.example.springhomework002.Model.Request.ProductRequest;
import com.example.springhomework002.Repositery.ProductRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    private final ProductRepo repo;

    public ProductServiceImp(ProductRepo repo) {
        this.repo = repo;
    }


    @Override
    public List<Product> getAllProduct() {
        return repo.getAllProduct();
    }

    @Override
    public Product getProductId(Integer id) {
        return repo.getProductId(id);
    }

    @Override
    public Product insertPro(ProductRequest productRequest) {
        return repo.insertPro(productRequest);
    }

    @Override
    public void deleteProduct(Integer id) {
         repo.deleteProduct(id);
    }

    @Override
    public Product updatebyID(Integer id,ProductRequest productRequest) {
        return repo.updateProduct(id,productRequest);
    }
}
