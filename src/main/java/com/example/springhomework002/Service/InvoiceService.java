package com.example.springhomework002.Service;

import com.example.springhomework002.Model.Entity.Invoice;
import com.example.springhomework002.Model.Request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    Invoice insertInvoice(InvoiceRequest invoiceRequest);

    Invoice getInvoiceId(Integer invoiceId);

    List<Invoice> getAllInvoice();

    void deleteInvoice(Integer id);

    Invoice updateInvoice(Integer id, InvoiceRequest invoiceRequest);
}
