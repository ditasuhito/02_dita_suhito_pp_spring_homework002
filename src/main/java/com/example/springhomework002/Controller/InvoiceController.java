package com.example.springhomework002.Controller;

import com.example.springhomework002.Model.Entity.Customer;
import com.example.springhomework002.Model.Entity.Invoice;
import com.example.springhomework002.Model.Request.InvoiceRequest;
import com.example.springhomework002.Model.Response.Responder;
import com.example.springhomework002.Service.InvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
@RestController
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/api/v1/invoice/get-all-invoice ")
    public ResponseEntity<?> getAllInvoice(){
        Responder responMessage = new Responder<List<Customer>>();
        responMessage.setMes("Customer Fetched All Sucessfully");
        responMessage.setPayload(invoiceService.getAllInvoice());
        return ResponseEntity.ok().body(responMessage);
    }

    @GetMapping("/api/v1/invoice/get-invoice-by-id/{id}")
    public ResponseEntity<?> getInvoiceId (@PathVariable Integer id){
        if (invoiceService.getInvoiceId(id) !=null){
            Invoice cus = invoiceService.getInvoiceId(id);
            return ResponseEntity.ok(new Responder<>(
                    LocalDateTime.now(), HttpStatus.OK, "Succesfully fetched author", cus));
        }
        else {

            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping("/api/v1/invoice/add-new-invoice")
    public ResponseEntity<?> insertInvoice(@RequestBody InvoiceRequest invoiceRequest){
        return ResponseEntity.ok(new Responder<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Add Successfully",
                invoiceService.insertInvoice(invoiceRequest)

        ));

    }
    @PutMapping("/api/v1/invoice/update-invoice-by-id/{id} ")
    public ResponseEntity<?> updateInvoice(@PathVariable Integer id, @RequestBody InvoiceRequest invoiceRequest){
        if (invoiceService.updateInvoice(id,invoiceRequest) != null) {
            return ResponseEntity.ok(new Responder<>(
                    LocalDateTime.now(),
                    HttpStatus.OK,
                    "Update Successful ", invoiceService.updateInvoice(id, invoiceRequest)
            ));
        }else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/api/v1/invoice/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteInvoice(@PathVariable Integer id) {

        invoiceService.deleteInvoice(id);
        return ResponseEntity.ok(new Responder<Invoice>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Deleted Successful",
                null
        ));
    }
}
