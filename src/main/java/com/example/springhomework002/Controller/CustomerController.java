package com.example.springhomework002.Controller;

import com.example.springhomework002.Model.Entity.Customer;
import com.example.springhomework002.Model.Request.CustomerRequest;
import com.example.springhomework002.Model.Response.Responder;
import com.example.springhomework002.Service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/api/v1/customer/get-all-customer")
    public ResponseEntity<?> getAllCustomer(){
        Responder responMessage = new Responder<List<Customer>>();
        responMessage.setMes("Customer Fetched All Sucessfully");
        responMessage.setPayload(customerService.getAllCustomer());
        return ResponseEntity.ok().body(responMessage);
    }

    @GetMapping("/api/v1/customer/get-customer-by-id/{id}")
    public ResponseEntity<?> getCustomerId (@PathVariable Integer id){
        if (customerService.getCustomerId(id) !=null){
            Customer cus = customerService.getCustomerId(id);
            return ResponseEntity.ok(new Responder<>(
                    LocalDateTime.now(), HttpStatus.OK, "Succesfully fetched author", cus));
        }
        else {

            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping("/api/v1/customer/add-new-customer")
    public ResponseEntity<?> insertCus(@RequestBody CustomerRequest customerRequest){
        return ResponseEntity.ok(new Responder<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Add Successfully",
                customerService.insertCustomer(customerRequest)

        ));

    }
    @PutMapping ("/api/v1/customer/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCus(@PathVariable Integer id, @RequestBody CustomerRequest customerRequest){
        if (customerService.updatebyID(id,customerRequest) != null) {
            return ResponseEntity.ok(new Responder<>(
                    LocalDateTime.now(),
                    HttpStatus.OK,
                    "Update Successful ", customerService.updatebyID(id, customerRequest)
            ));
        }else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/api/v1/customer/delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Integer id) {

        customerService.deleteCustomer(id);
        return ResponseEntity.ok(new Responder<Customer>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Deleted Successful",
                null
        ));
    }

}
