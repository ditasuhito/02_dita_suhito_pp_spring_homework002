package com.example.springhomework002.Controller;

import com.example.springhomework002.Model.Entity.Product;
import com.example.springhomework002.Model.Request.ProductRequest;
import com.example.springhomework002.Model.Response.Responder;
import com.example.springhomework002.Service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDateTime;
import java.util.List;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;

    }
    @GetMapping("/api/v1/product/get-all-product")
    public ResponseEntity<?> getAllProduct(){
        Responder responMessage = new Responder<List<Product>>();
        responMessage.setMes("Product Fetched All Sucessfully");
        responMessage.setPayload(productService.getAllProduct());
        return ResponseEntity.ok().body(responMessage);
    }
    @GetMapping("/api/v1/product/get-product-by-id/{id}")
    public  ResponseEntity<?> getProductId(@PathVariable Integer id){
        if (productService.getProductId(id) !=null){
        Product pro = productService.getProductId(id);
        return ResponseEntity.ok(new Responder<>(
                LocalDateTime.now(), HttpStatus.OK, "Succesfully fetched Product", pro));
    }else {
            return ResponseEntity.notFound().build();
        }
    }
    @PostMapping("/api/v1/product/add-new-product")
    public ResponseEntity<?> insertPro(@RequestBody ProductRequest productRequest){
        return ResponseEntity.ok(new Responder<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully Insert Product",
                productService.insertPro(productRequest)))
                ;
    }
    @PutMapping ("/api/v1/product/update-product-by-id/{id}")
    public ResponseEntity<?> updatePro(@PathVariable Integer id, @RequestBody ProductRequest productRequest) {
        if (productService.updatebyID(id, productRequest) != null) {
            return ResponseEntity.ok(new Responder<>(
                    LocalDateTime.now(),
                    HttpStatus.OK,
                    "Successfully Update Product",
                    productService.updatebyID(id, productRequest)

            ));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/api/v1/product/delete-product-by-id/{id}")
    public ResponseEntity<?> deletePro(@PathVariable Integer id) {
        productService.deleteProduct(id);
        return ResponseEntity.ok(new Responder<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully Deleted Product",null

        ));
    }

}

