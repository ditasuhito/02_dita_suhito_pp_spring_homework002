package com.example.springhomework002.Repositery;

import com.example.springhomework002.Model.Entity.Customer;
import com.example.springhomework002.Model.Entity.Product;
import com.example.springhomework002.Model.Request.ProductRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProductRepo {
    @Select("""
        SELECT * FROM product_tb
        """ )
    @Results(
            id = "ProductMap", value = {
            @Result(property = "product_id", column = "product_id"),
            @Result(property = "product_name", column = "product_name"),
            @Result(property = "product_price", column = "product_price"),
    }
    )
    List<Product> getAllProduct();

    @Delete("""
        DELETE FROM product_tb WHERE product_id =#{id};
        """)
    void deleteProduct(Integer id);



    @Select("""
        SELECT * FROM product_tb WHERE product_id=#{id};
        """)
    Product getProductId(Integer id);



    @Update("""
                UPDATE product_tb
                SET product_name = #{product.name}, product_price= #{product.price}
                WHERE product_id = #{id}
                returning *
            """)
    @ResultMap("ProductMap")
    Product updateProduct(Integer id,@Param("product")ProductRequest productRequest);

    @Select("""
          INSERT INTO product_tb ( product_name , product_price)
          VALUES (#{product.name} , #{product.price});
          returning *
            """)
    @ResultMap("ProductMap")
    Product insertPro( @Param("product") ProductRequest productRequest);
}
