package com.example.springhomework002.Repositery;

import com.example.springhomework002.Model.Entity.Invoice;
import com.example.springhomework002.Model.Request.InvoiceRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface InvoiceRepo {

    @Select("""
          INSERT INTO invoice_tb ( invoice_id , invoice_date, customer_id)
          VALUES (#{invoice.name} , #{invoice.date}, #{customer.id})
          returning *
            """)
    @ResultMap("InvoiceMap")
    Invoice insertData(InvoiceRequest invoiceRequest);

    Invoice getById(Integer id);

    List<Invoice> getAllInvoice();

    void deleteInvoice(Integer id);

    Invoice updateInvoice(Integer id, InvoiceRequest invoiceRequest);
}
