package com.example.springhomework002.Repositery;

import com.example.springhomework002.Model.Entity.Customer;
import com.example.springhomework002.Model.Request.CustomerRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface CustomerRepo {

    //SELECT
    @Select("""
        SELECT * FROM customer_tb
        """ )
    @Results(
            id = "customerMap", value = {
            @Result(property = "customer_id", column = "customer_id"),
            @Result(property = "customer_name", column = "customer_name"),
            @Result(property = "customer_address", column = "customer_address"),
            @Result(property = "customer_phone", column = "customer_phone"),
    }
    )
    List<Customer> getAllCustomer();



    @Delete("""
        DELETE FROM customer_tb WHERE customer_id = #{id}
        """)
    @ResultMap("customerMap")
    void deleteCustomer(Integer id);


    @Select("""
        SELECT * FROM customer_tb WHERE customer_id = #{id};
        """)
    Customer getByiD(Integer id);



    @Select("""
          INSERT INTO customer_tb ( customer_name , customer_address, customer_phone)
          VALUES (#{customer.name} , #{customer.address}, #{customer.phone})
          returning *
            """)
    @ResultMap("customerMap")
    Customer insertData(@Param("customer") CustomerRequest customerRequest);




    @Select("""
                UPDATE customer_tb
                SET customer_name = #{customer.name}, customer_address = #{customer.address}, customer_phone = #{customer.phone}
                WHERE customer_id = #{id}
                returning *
            """)
    @ResultMap("customerMap")
   Customer updatebyID(Integer id, @Param("customer")CustomerRequest customerRequest);
}
