CREATE TABLE product_tb
(
    product_id serial PRIMARY KEY NOT NULL, product_name varchar(50) NOT NULL, product_price varchar(50) NOT NULL

);
CREATE TABLE invoice_tb(
                           invoice_id serial PRIMARY KEY,
                           invoice_date DATE NOT NULL,
                           customer_id INT NOT NULL,
                           FOREIGN KEY (customer_id) REFERENCES customer_tb (customer_id) ON DELETE CASCADE ON UPDATE CASCADE

);
CREATE TABLE customer_tb(
                            customer_id serial PRIMARY KEY,
                            customer_name varchar(50),
                            customer_address varchar(50),
                            customer_phone varchar(10)

);
CREATE TABLE invoice_detail_db
(
    id serial PRIMARY KEY,
    invoice_id INT NOT NULL,
    product_id  INT NOT NULL,
    FOREIGN KEY (invoice_id) REFERENCES invoice_tb (invoice_id) ON DELETE CASCADE ON UPDATE CASCADE ,
    FOREIGN KEY (product_id) REFERENCES product_tb (product_id) ON DELETE CASCADE ON UPDATE CASCADE
)
